from kivy.uix.gridlayout import GridLayout
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.graphics import Color, Rectangle
import random
import time
from kivy.clock import Clock
import string
from kivy.animation import Animation
from kivy.core.audio import SoundLoader
from kivyparticle import ParticleSystem
from kivy.uix.widget import Widget

from kivy.uix.scrollview import ScrollView
from kivy.uix.screenmanager import ScreenManager, Screen,WipeTransition,FallOutTransition 
from kivy.core.window import Window





"""This module contains code from
Think Python by Allen B. Downey
http://thinkpython.com

Copyright 2012 Allen B. Downey
License: GNU GPLv3 http://www.gnu.org/licenses/gpl.html

"""



class Card(object):
    """Represents a standard playing card.
    
    Attributes:
      suit: integer 0-3
      rank: integer 1-13
    """

    suit_names = ["Cross", "Square", "Triangle", "Circle","Star","Whot"]
    rank_names = [None, "1", "2", "3", "4", "5", "6", "7", 
              "8", "9", "10", "11", "12", "13","14"]

    def __init__(self, suit=0, rank=2):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        """Returns a human-readable string representation."""
        return '%s of %s' % (self.rank_names[self.rank],
                             Card.suit_names[self.suit])

    def __cmp__(self, other):
        """Compares this card to other, first by suit, then rank.

        Returns a positive number if this > other; negative if other > this;
        and 0 if they are equivalent.
        """
        t1 = self.suit, self.rank
        t2 = other.suit, other.rank
        return cmp(t1, t2)


class DemoParticle(Widget):
    def __init__(self, **kwargs):
        super(DemoParticle, self).__init__(**kwargs)
        self.sun = ParticleSystem('media/sun.pex')
        self.drugs = ParticleSystem('media/drugs.pex')
        self.jellyfish = ParticleSystem('media/jellyfish.pex')
        self.fire = ParticleSystem('media/fire.pex')

        self.current = None
        self._show(self.sun)

    def on_touch_down(self, touch):
        self.current.emitter_x = float(touch.x)
        self.current.emitter_y = float(touch.y)

    def on_touch_move(self, touch):
        self.current.emitter_x = float(touch.x)
        self.current.emitter_y = float(touch.y)

    def show_sun(self, b):
        self._show(self.sun)

    def show_drugs(self, b):
        self._show(self.drugs)

    def show_jellyfish(self, b):
        self._show(self.jellyfish)

    def show_fire(self, b):
        self._show(self.fire)

    def _show(self, system):
        if self.current:
            self.remove_widget(self.current)
            self.current.stop(True)
        self.current = system

        self.current.emitter_x = 300.0
        self.current.emitter_y = 300.0
        self.add_widget(self.current)
        self.current.start()


class Deck(object):
    """Represents a deck of cards.

    Attributes:
      cards: list of Card objects.
    """
    
    def __init__(self):
        self.cards = []
        for suit in range(6):
            for rank in range(1, 15):
            
                if suit == 0 and rank == 4:
                    pass
                elif  suit == 0 and rank == 6:
                    pass
                elif  suit == 0 and rank == 8:
                    pass
                elif  suit == 0 and rank == 9:
                    pass
                elif  suit == 0 and rank == 10:
                    pass
                elif suit == 1 and rank == 4:
                    pass
                elif  suit == 1 and rank == 6:
                    pass
                elif  suit == 1 and rank == 8:
                    pass
                elif  suit == 1 and rank == 9:
                    pass
                elif  suit == 1 and rank == 10:
                    pass
                elif  suit == 2 and rank == 6:
                    pass
                elif  suit == 2 and rank == 9:
                    pass
                elif  suit == 3 and rank == 6:
                    pass
                elif  suit == 3 and rank == 9:
                    pass
                elif  suit == 4 and rank == 6:
                    pass
                elif  suit == 4 and rank >8:
                    pass
                elif  suit == 5 and rank == 1:
                    pass
                elif  suit == 5 and rank == 2:
                    pass
                elif  suit == 5 and rank == 5:
                    pass
                elif  suit ==5 and rank >8:
                    pass
                
                else:
                    card = Card(suit, rank)
                    self.cards.append(card)

    def __str__(self):
        res = []
        for card in self.cards:
            res.append(str(card))
        return '\n'.join(res)

    def add_card(self, card):
        """Adds a card to the deck."""
        self.cards.append(card)

    def remove_card(self, card):
        """Removes a card from the deck."""
        if card  in self.cards:
            self.cards.remove(card)
        else:
            print 'sorry'
            print card

    def pop_card(self, i=-1):
        """Removes and returns a card from the deck.

        i: index of the card to pop; by default, pops the last card.
        """
        return self.cards.pop(i)

    def shuffle(self):
        """Shuffles the cards in this deck."""
        random.shuffle(self.cards)

    def sort(self):
        """Sorts the cards in ascending order."""
        self.cards.sort()

    def move_cards(self, hand, num):
        """Moves the given number of cards from the deck into the Hand.

        hand: destination Hand object
        num: integer number of cards to move
        """
        '''for i in range(num):
            hand.add_card(self.pop_card())
            '''
        nHands = len(hand)
        for i in range(num):
            #if self.isEmpty(): break
            card = self.pop_card()
            hands = hand[i % nHands]
            hands.add_card(card)


class Hand(Deck):
    """Represents a hand of playing cards."""
    
    def __init__(self, label=''):
        self.cards = []
        self.label = label
    def add_card(self, card):
        """Adds a card to the deck."""
        self.cards.append(card)


def find_defining_class(obj, method_name):
    """Finds and returns the class object that will provide 
    the definition of method_name (as a string) if it is
    invoked on obj.

    obj: any python object
    method_name: string method name
    """
    for ty in type(obj).mro():
        if method_name in ty.__dict__:
            return ty
    return None
    
class game(Screen):
    
    def __init__(self,**kwargs):
            
            self.gridmain=GridLayout(rows=3)
            self.sound =SoundLoader.load('01 Hello 1.wav')
            self.sound.play()
            self.turn=1
            self.funnyabc=[];
            super(game, self).__init__(**kwargs) 
            with self.gridmain.canvas.before:
                #Color(1, 1, 0, 1, mode='rgba')
            
            
                Rectangle(pos=self.pos,size=Window.size,source= 'maintable.png')
            self.deck = Deck()
            self.deck.shuffle()
            #self.deck.removeCard(Card(12,1))
            self.hand1 = Hand("frank")
            self.hand2 = Hand("kings")
            #print len(self.deck.cards)
            self.comp = Hand("comp")
            self.deck.move_cards([self.hand1,self.hand2], 10)
            #self.deck.deal([self.hand2], 5)
            self.deck.move_cards([self.comp], 1)
            print self.deck
            print len(str(self.deck.cards))
            print 1
            self.add_widget(self.gridmain)
            self.decklayout=GridLayout(rows=1)
            self.handgrid1=GridLayout(rows=1)
            self.handgrid2cols=GridLayout(rows=1,spacing=10, size_hint_x=None)
            
            self.handgrid2=GridLayout(rows=1,spacing=10, size_hint_x=None)
            self.handgrid2.bind(minimum_width=self.handgrid2.setter('width'))
            #self.sv.add_widget(self.handgrid2)
            
            self.gridmain.add_widget(self.handgrid1)
            self.gridmain.add_widget(self.decklayout)
            self.bbtt=Label(markup=True);
            self.bbtt2=Button(markup=True);
            
            self.gridmain.add_widget(self.handgrid2cols)
            self.deal=Button(text='click to start game',size_hint=(1,1))
            self.deal2=Button(background_normal='whot/back.png',size_hint=(None,None),markup=True,text='[color=333333][size=30]'+str(len(self.deck.cards))+'[/size][/color]')
            self.decklayout.add_widget(self.deal)
            self.deal.bind(on_press= lambda x:self.tryme())
            self.deal2.bind(on_press= lambda x:self.takefromhand())
            self.label=Label()
            self.bbtt2.bind(on_press= lambda x:self.shufinhand())
            Clock.schedule_interval(self.update, 7 / 3)
            #print len(self.deck.cards)
            
    def tryme(self):
            self.handgrid2cols.add_widget(self.bbtt)
            self.handgrid2cols.add_widget(self.handgrid2)

            self.handgrid2cols.add_widget(self.bbtt2)
            
            for a in self.hand2.cards:
                
                
                self.handgrid1.add_widget(Button(text=str(a)))
                #Clock.schedule_once(handgrid1.add_widget(Button(text=str(a))),2)
                #time.sleep(0.2)
                #print len(self.hand2.cards)
                #self.add_widget(handgrid1)
                #handgrid2.add_widget(Button(text=str(a)))
            
            for a in range(len(self.hand1.cards)):
                self.funnyabc.append(self.hand1.cards[a])
            
                if a>=4:
                    break
                else:
                    abc=self.hand1.cards[a]
                    #self.funnyabc.append
                    self.btu=Button(markup=True,text='[color=666666][size=10]'+'!'+str(abc)+'.png'+'![/size][/color]',background_normal='whot/'+str(abc)+'.png',allow_stretch=True,size_hint_x=None, )    
                    self.handgrid2.add_widget(self.btu)
                    self.btu.bind(on_press= lambda a,b=self.btu.text,: self.callback(b))
                #time.sleep(0.2)
            #self.add_widget(handgrid1)
            #self.add_widget(decklayout)
            #self.add_widget(handgrid2)
            self.decklayout.remove_widget(self.deal)
            self.decklayout.add_widget(self.deal2)
            self.decklayout.add_widget(self.label)
            self.btn1=Button(size_hint=(None,None),pos= (0,0))
            for a in self.comp.cards:
                
                self.btn1.background_normal='whot/'+str(a)+'.png'
                #self.a=a
                self.decklayout.add_widget(self.btn1)
                #self.btn1.text=a
            #print len(self.hand1.cards)
            print len(self.deck.cards)
            
            
    def callback(self,b):
        if self.turn == 1:
            self.wasteoftime(b)
            
    
        #START TO IDENTIFY WHAT LABEL VALUE IS 
    def wasteoftime(self,b):
        self.label.text='player 1'
        a=self.comp.cards[0]
        c=string.split(str(a), 'of')
        print 'it starts now'
        print c[1]
        print c[0]
        print 'it ends now'
        
        if c[1] == ' Cross':
            c[1]=0
        elif c[1] == ' Square':
            c[1]=1
        elif c[1] == ' Triangle':
            c[1]=2
        elif c[1] == ' Circle':
            c[1]=3
        elif c[1] == ' Star':
            c[1]=4
        elif c[1] == ' Whot':
            c[1]=5
        #data from the default hand = self.lolsplit
        self.lolsplit=string.split(b, '!')
        print self.lolsplit[0] 
        print self.lolsplit[1] 
        self.lolsplit2=string.split(self.lolsplit[1] , 'of')
        
        print self.lolsplit2[1] + 'this is suite aka shapes'
        print self.lolsplit2[0]  + 'this is rank aka numbers'
        print len(self.hand1.cards)
        print 1234
        
        if self.lolsplit2[1] == ' Cross.png':
            self.lolsplit2[1]=0
        elif self.lolsplit2[1] == ' Square.png':
            self.lolsplit2[1]=1
        elif self.lolsplit2[1] == ' Triangle.png':
            self.lolsplit2[1]=2
        elif self.lolsplit2[1] == ' Circle.png':
            self.lolsplit2[1]=3
        elif self.lolsplit2[1] == ' Star.png':
            self.lolsplit2[1]=4
        elif self.lolsplit2[1] == ' Whot.png':
            self.lolsplit2[1]=5
        #elif self.lolsplit2[1] == ' Spades.png':
            #self.lolsplit2[1]=4
        #elif self.lolsplit2[1] == 'Whot.png':
            #self.lolsplit2[1]=5
        print str(self.lolsplit2[1]) + 'this is suite aka shapes'
        #self.hand2.removeCard( Card(int(self.lolsplit2[1]),int(self.lolsplit2[0])))
        #if shape = shape or number = number card good
        if c[1]==self.lolsplit2[1] or c[0] == self.lolsplit2[0]:
            self.sound =SoundLoader.load('goodcard.wav')
            if int(self.lolsplit2[0])==2:
                self.turn=1
                if  len(self.deck.cards) < 1:
                    pass
                else:#condition for pick  2 starts here
                    if len(self.deck.cards)< 2:
                        for b in range(1):
                            self.hand2.add_card(self.deck.pop_card())
                    else:
                    # waith
                        for b in range(2):
                            
                            self.hand2.add_card(self.deck.pop_card())
                    self.him(b)
                    
                    print 1234
            elif int(self.lolsplit2[0])==5:
                self.turn=1
                if  len(self.deck.cards) < 1:
                    pass
                else:#  condition for else starts here 
                # waith
                    if len(self.deck.cards)< 2:
                        for b in range(1):
                            self.hand2.add_card(self.deck.pop_card())
                    else:
                        for b in range(3):
                            self.hand2.add_card(self.deck.pop_card())
                    self.him(b)
            elif int(self.lolsplit2[0])==1:
                self.turn=1
                self.him(b)
            elif int(self.lolsplit2[0])==14:
                if  len(self.deck.cards) < 1:
                    print 'end game'
                else:
                    self.turn=1
                    
                    # waith
                    
                    self.hand2.add_card(self.deck.pop_card())
                    self.him(b)
            else:
                self.testhel='[color=666666][size=10]!'+str(Card(int(self.lolsplit2[1]),int(self.lolsplit2[0])))+'.png![/size][/color]';
                self.hand1.remove_card(Card(int(self.lolsplit2[1]),int(self.lolsplit2[0])))
                self.sound.play()
            
                self.comp.add_card(Card(int(self.lolsplit2[1]),int(self.lolsplit2[0])))
                del self.comp.cards[0]
                print str(len(self.comp.cards)) + 'card was not aded'
                print str(self.lolsplit2[1]) + 'thi is shapes'
                print str(self.lolsplit2[0]) + "this is numbers"
                #for child in self.decklayout.children[:]:
                    #self.decklayout.remove_widget(child)
                #time.sleep(1)
                anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                anim += Animation(opacity=1,  duration=0.8)
                anim.start(self.btn1)
                for a in self.comp.cards:
                    
                    self.btn1.background_normal='whot/'+str(a)+'.png'
                #anim.cancel(self.btn1)
                for child in self.handgrid2.children[:]:
                    fooks=child;
                    def tete():
                        self.handgrid2.remove_widget(fooks)
                        
                    if str(child.text)==str(self.testhel):
                        waiting=int(child.x)+400
                        waiting2=int(child.y)+400
                        anim = Animation(x=waiting, y=waiting2)
                        anim.bind(on_complete=tete())
                        anim.start(child)
                        #print child.x;
                        #print self.testhel;
                    self.handgrid2.remove_widget(fooks)
                    
                        
                    
                    
                
                for a in self.hand1.cards:
                    self.btu=Button(markup=True,text='[color=666666][size=10]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+str(a)+'.png',size_hint=(None,None), allow_stretch=True,size_hint_y=None, )       
                    self.handgrid2.add_widget(self.btu)
                    self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                print len(self.hand1.cards)
                #self.add_widget(self.handgrid2)
                self.turn=2
                print 1234
        else:
            self.sound = SoundLoader.load('badcard.wav')
            self.sound.play()
    
    def him(self,b):
        for child in self.handgrid1.children[:]:
            anim = Animation(x=100, y=100)
            anim.start(child)
            self.handgrid1.remove_widget(child)
            print len(self.hand2.cards)
        self.counter=0
        for a in self.hand2.cards:
            if self.counter >=4:
                break
            else:
                self.btu=Button(markup=True,text='[color=666666][size=10]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back'+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )    
                self.handgrid1.add_widget(self.btu)
                self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                #time.sleep(1)
            self.counter+1
                                
                    
                    # now
        
        self.hand1.remove_card(Card(int(self.lolsplit2[1]),int(self.lolsplit2[0])))
        self.sound.play()
                    
        self.comp.add_card(Card(int(self.lolsplit2[1]),int(self.lolsplit2[0])))
        del self.comp.cards[0]
                    
                    #for child in self.decklayout.children[:]:
                        #self.decklayout.remove_widget(child)
                    #time.sleep(1)
        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
        anim += Animation(opacity=1,  duration=0.8)
        anim.start(self.btn1)
        for a in self.comp.cards:
            
            self.btn1.background_normal='whot/'+str(a)+'.png'
        #anim.cancel(self.btn1)
        for child in self.handgrid2.children[:]:
            self.handgrid2.remove_widget(child)
        #time.sleep(1)
        for a in range(len(self.hand1.cards)):
            self.funnyabc.append(self.hand1.cards[a])
            print self.funnyabc    
            if a>=4:
                break
            else:
                abc=self.hand2.cards[a-1]
            self.btu=Button(markup=True,text='[color=666666][size=10]'+'!'+str(abc)+'.png'+'![/size][/color]',background_normal='whot/'+str(abc)+'.png',size_hint=(None,None), allow_stretch=True,size_hint_y=None, )    
            self.handgrid2.add_widget(self.btu)
            self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
        print len(self.hand1.cards)
        self.turn=1
                    #self.add_widget(self.handgrid2)
    def trythis(self,dt):
        if self.turn == 2:
            
            self.label.text='computer'
            
            
            cardsa = [50,51]
            cardsa2 = []
            q=0
            z='2'
            y='4'
            o=y+z
            
            a=self.comp.cards[0]
            c=string.split(str(a), 'of')
            print 'it starts now'
            print c[1]
            print c[0]
            print 'it ends now'
            
            if c[1] == ' Cross':
                c[1]=70
            elif c[1] == ' Square':
                c[1]=71
            elif c[1] == ' Triangle':
                c[1]=72
            elif c[1] == ' Circle':
                c[1]=73
            elif c[1] == ' Star':
                c[1]=74
            elif c[1] == ' Whot':
                c[1]=75
                
            
            for a in self.hand2.cards:  #the hand loop
                #print a 
                cj=string.split(str(a), 'of') #split the hand into 2
                cardsa.append(cj[0]) # append the split content to a new list
                k=string.split(cj[1], ' ') #remove the space on the first split
                cardsa.append(k[1])  # also append it
                #print c[1]
                #cardsa2.append(c[1])
                #print c[0]
            for w in cardsa: #split the content of each lists entry
                q+=1 # start count
                if w=='a':#make w a number when it sees a string
                    w=41
                if w == 'b':
                    w="42"
                if w == 'Cross':
                    w=70
                elif w == 'Square':
                    w=71
                elif w == 'Triangle':
                    w=72
                elif w == 'Circle':
                    w=73
                elif w == 'Star':
                    w=74
                elif w == 'Whot':
                    w=75
                if  int(w) == int(c[0]):# check the rank to c if its a match
                    if int(w)==2:
                        self.turn=2
                        if  len(self.deck.cards) < 1:# start the computer  condition for amount of cards
                            pass
                        else:
                            # waith
                            if len(self.deck.cards)< 2:
                                for b in range(1):
                                    self.hand2.add_card(self.deck.pop_card())
                            else:
                                for b in range(2):
                                    self.hand1.add_card(self.deck.pop_card())
                            for child in self.handgrid2.children[:]:
                                self.handgrid2.remove_widget(child)
                                print len(self.hand2.cards)
                            for a in self.hand1.cards:
                                self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+str(a)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None,)       
                                self.handgrid2.add_widget(self.btu)
                                self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                                #time.sleep(1)
                            print "ok1"
                            print q
                            print w
                            
                            print int(cardsa[q-1])
                            y=q - 1
                            print q-2
                            print q-1
                            print q
                            if cardsa[q] == 'Cross':
                                cardsa[q]=0
                            elif cardsa[q] == 'Square':
                                cardsa[q]=1
                            elif cardsa[q] == 'Triangle':
                                cardsa[q]=2
                            elif cardsa[q] == 'Circle':
                                cardsa[q]=3
                            elif cardsa[q] == 'Star':
                                cardsa[q]=4
                            elif cardsa[q] == 'Whot':
                                cardsa[q]=5
                            print int(cardsa[q])
                            print "ok end"
                            self.hand2.remove_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                            self.comp.add_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                            del self.comp.cards[0]
                            anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                            anim += Animation(opacity=1,  duration=0.8)
                            anim.start(self.btn1)
                            for a in self.comp.cards:
                            
                                self.btn1.background_normal='whot/'+str(a)+'.png'
                        #anim.cancel(self.btn1)
                            for child in self.handgrid1.children[:]:
                                self.handgrid1.remove_widget(child)
                            print len(self.hand2.cards)
                            for a in self.hand2.cards:
                                self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )       
                                self.handgrid1.add_widget(self.btu)
                                #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                            print len(self.hand2.cards)
                        
                        
                        
                        
                        break
                        
                        print 1234
                    elif int(w)==5:
                        self.turn=2
                        # waith
                        if  len(self.deck.cards) < 1: #second condition for if the card is 5
                            pass
                        else:
                            if len(self.deck.cards)< 2:
                                for b in range(1):
                                    self.hand2.add_card(self.deck.pop_card())
                            else:
                                for b in range(3):
                                    self.hand1.add_card(self.deck.pop_card())
                            for child in self.handgrid2.children[:]:
                                self.handgrid2.remove_widget(child)
                                print len(self.hand2.cards)
                            for a in self.hand1.cards:
                                self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+str(a)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                                self.handgrid2.add_widget(self.btu)
                                self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                                #time.sleep(1)
                            print "ok1"
                            print q
                            print w
                            
                            print int(cardsa[q-1])
                            y=q - 1
                            print q-2
                            print q-1
                            print q
                            if cardsa[q] == 'Cross':
                                cardsa[q]=0
                            elif cardsa[q] == 'Square':
                                cardsa[q]=1
                            elif cardsa[q] == 'Triangle':
                                cardsa[q]=2
                            elif cardsa[q] == 'Circle':
                                cardsa[q]=3
                            elif cardsa[q] == 'Star':
                                cardsa[q]=4
                            elif cardsa[q] == 'Whot':
                                cardsa[q]=5
                            print int(cardsa[q])
                            print "ok end"
                            self.hand2.remove_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                            self.comp.add_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                            del self.comp.cards[0]
                            anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                            anim += Animation(opacity=1,  duration=0.8)
                            anim.start(self.btn1)
                            for a in self.comp.cards:
                            
                                self.btn1.background_normal='whot/'+str(a)+'.png'
                        #anim.cancel(self.btn1)
                            for child in self.handgrid1.children[:]:
                                self.handgrid1.remove_widget(child)
                            print len(self.hand2.cards)
                            for a in self.hand2.cards:
                                self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                                self.handgrid1.add_widget(self.btu)
                                #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                            print len(self.hand2.cards)
                            
                            
                            
                            
                            break
                            
                            
                            print 1234
                    elif int(w)==1:
                        self.turn=2
                        
                        
                            #time.sleep(1)
                        print "ok1"
                        print q
                        print w
                        
                        print int(cardsa[q-1])
                        y=q - 1
                        print q-2
                        print q-1
                        print q
                        if cardsa[q] == 'Cross':
                            cardsa[q]=0
                        elif cardsa[q] == 'Square':
                            cardsa[q]=1
                        elif cardsa[q] == 'Triangle':
                            cardsa[q]=2
                        elif cardsa[q] == 'Circle':
                            cardsa[q]=3
                        elif cardsa[q] == 'Star':
                            cardsa[q]=4
                        elif cardsa[q] == 'Whot':
                            cardsa[q]=5
                        print int(cardsa[q])
                        print "ok end"
                        self.hand2.remove_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                        self.comp.add_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                        del self.comp.cards[0]
                        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                        anim += Animation(opacity=1,  duration=0.8)
                        anim.start(self.btn1)
                        for a in self.comp.cards:
                        
                            self.btn1.background_normal='whot/'+str(a)+'.png'
                    #anim.cancel(self.btn1)
                        for child in self.handgrid1.children[:]:
                            self.handgrid1.remove_widget(child)
                        print len(self.hand2.cards)
                        for a in self.hand2.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                            self.handgrid1.add_widget(self.btu)
                            #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print len(self.hand2.cards)
                        
                        
                        
                        
                        break
                        
                        
                        print 1234
                    elif int(w)==14:
                        self.turn=2
                        if  len(self.deck.cards) < 1: #condition for deck when on general market
                            pass
                        # waith
                        
                        else:
                            self.hand1.add_card(self.deck.pop_card())
                            for child in self.handgrid2.children[:]:
                                self.handgrid2.remove_widget(child)
                                print len(self.hand2.cards)
                            for a in self.hand1.cards:
                                self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+str(a)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                                self.handgrid2.add_widget(self.btu)
                                self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                                #time.sleep(1)
                            print "ok1"
                            print q
                            print w
                            
                            print int(cardsa[q-1])
                            y=q - 1
                            print q-2
                            print q-1
                            print q
                            if cardsa[q] == 'Cross':
                                cardsa[q]=0
                            elif cardsa[q] == 'Square':
                                cardsa[q]=1
                            elif cardsa[q] == 'Triangle':
                                cardsa[q]=2
                            elif cardsa[q] == 'Circle':
                                cardsa[q]=3
                            elif cardsa[q] == 'Star':
                                cardsa[q]=4
                            elif cardsa[q] == 'Whot':
                                cardsa[q]=5
                            print int(cardsa[q])
                            print "ok end"
                            self.hand2.remove_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                            self.comp.add_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                            del self.comp.cards[0]
                            anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                            anim += Animation(opacity=1,  duration=0.8)
                            anim.start(self.btn1)
                            for a in self.comp.cards:
                            
                                self.btn1.background_normal='whot/'+str(a)+'.png'
                        #anim.cancel(self.btn1)
                            for child in self.handgrid1.children[:]:
                                self.handgrid1.remove_widget(child)
                            print len(self.hand2.cards)
                            for a in self.hand2.cards:
                                self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )      
                                self.handgrid1.add_widget(self.btu)
                                #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                            print len(self.hand2.cards)
                            
                            
                            
                            
                            break
                        
                    else:
                        
                        print "ok1"
                        print q
                        print w
                        
                        print int(cardsa[q-1])
                        y=q - 1
                        print q-2
                        print q-1
                        print q
                        if cardsa[q] == 'Cross':
                            cardsa[q]=0
                        elif cardsa[q] == 'Square':
                            cardsa[q]=1
                        elif cardsa[q] == 'Triangle':
                            cardsa[q]=2
                        elif cardsa[q] == 'Circle':
                            cardsa[q]=3
                        elif cardsa[q] == 'Star':
                            cardsa[q]=4
                        elif cardsa[q] == 'Whot':
                            cardsa[q]=5
                        print int(cardsa[q])
                        print "ok end"
                        self.hand2.remove_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                        self.comp.add_card(Card(int(cardsa[q]),int(cardsa[q-1])))
                        del self.comp.cards[0]
                        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                        anim += Animation(opacity=1,  duration=0.8)
                        anim.start(self.btn1)
                        for a in self.comp.cards:
                        
                            self.btn1.background_normal='whot/'+str(a)+'.png'
                    #anim.cancel(self.btn1)
                        for child in self.handgrid1.children[:]:
                            self.handgrid1.remove_widget(child)
                        print len(self.hand2.cards)
                        for a in self.hand2.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )       
                            self.handgrid1.add_widget(self.btu)
                            #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print len(self.hand2.cards)
                        self.turn = 1
                        
                        
                        
                        break
                
                elif int(w) ==int(c[1]): #else check the suice 
                    if int(cardsa[q-2])==2:
                        self.turn=2
                        print int(cardsa[q-2])
                        print 'that is y 2'
                        
                        for b in range(2):
                            self.hand1.add_card(self.deck.pop_card())
                        for child in self.handgrid2.children[:]:
                            self.handgrid2.remove_widget(child)
                            print len(self.hand2.cards)
                        for a in self.hand1.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+str(a)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )       
                            self.handgrid2.add_widget(self.btu)
                            self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print "ok2"
                        print q
                        print w
                        if cardsa[q-1] == 'Cross':
                            cardsa[q-1]=0
                        elif cardsa[q-1] == 'Square':
                            cardsa[q-1]=1
                        elif cardsa[q-1] == 'Triangle':
                            cardsa[q-1]=2
                        elif cardsa[q-1] == 'Circle':
                            cardsa[q-1]=3
                        elif cardsa[q-1] == 'Star':
                            cardsa[q-1]=4
                        elif cardsa[q-1] == 'Whot':
                            cardsa[q-1]=5
                        print int(cardsa[q-2])
                        print int(cardsa[q-1])
                        print "ok2 end"
                        self.hand2.remove_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        self.comp.add_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        del self.comp.cards[0]
                        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                        anim += Animation(opacity=1,  duration=0.8)
                        anim.start(self.btn1)
                        #time.sleep(1)
                        for a in self.comp.cards:
                        
                            self.btn1.background_normal='whot/'+str(a)+'.png'
                    #anim.cancel(self.btn1)
                        for child in self.handgrid1.children[:]:
                            self.handgrid1.remove_widget(child)
                        print len(self.hand2.cards)
                        for a in self.hand2.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None,)     
                            self.handgrid1.add_widget(self.btu)
                            #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print len(self.hand2.cards)
                        self.turn = 2
                        time.sleep(1)
                        break
                                                
                    elif int(cardsa[q-2])==5:
                        self.turn=2
                        print int(cardsa[q-2])
                        print 'that is y 5'
                        
                        for b in range(3):
                            self.hand1.add_card(self.deck.pop_card())
                        for child in self.handgrid2.children[:]:
                            self.handgrid2.remove_widget(child)
                            print len(self.hand2.cards)
                        for a in self.hand1.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+str(a)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None,)        
                            self.handgrid2.add_widget(self.btu)
                            self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print "ok2"
                        print q
                        print w
                        if cardsa[q-1] == 'Cross':
                            cardsa[q-1]=0
                        elif cardsa[q-1] == 'Square':
                            cardsa[q-1]=1
                        elif cardsa[q-1] == 'Triangle':
                            cardsa[q-1]=2
                        elif cardsa[q-1] == 'Circle':
                            cardsa[q-1]=3
                        elif cardsa[q-1] == 'Star':
                            cardsa[q-1]=4
                        elif cardsa[q-1] == 'Whot':
                            cardsa[q-1]=5
                        print int(cardsa[q-2])
                        print int(cardsa[q-1])
                        print "ok2 end"
                        self.hand2.remove_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        self.comp.add_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        del self.comp.cards[0]
                        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                        anim += Animation(opacity=1,  duration=0.8)
                        anim.start(self.btn1)
                        #time.sleep(1)
                        for a in self.comp.cards:
                        
                            self.btn1.background_normal='whot/'+str(a)+'.png'
                    #anim.cancel(self.btn1)
                        for child in self.handgrid1.children[:]:
                            self.handgrid1.remove_widget(child)
                        print len(self.hand2.cards)
                        for a in self.hand2.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                            self.handgrid1.add_widget(self.btu)
                            self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print len(self.hand2.cards)
                        self.turn = 2
                        time.sleep(1)
                        break
                                                
                            # waith
                            
                    elif int(cardsa[q-2])==14:
                        self.turn=2
                        print int(cardsa[q-2])
                        print 'that is y 14'
                        
                         
                        self.hand1.add_card(self.deck.pop_card())
                        for child in self.handgrid2.children[:]:
                            self.handgrid2.remove_widget(child)
                            print len(self.hand2.cards)
                        for a in self.hand1.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+str(a)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )       
                            self.handgrid2.add_widget(self.btu)
                            self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print "ok2"
                        print q
                        print w
                        if cardsa[q-1] == 'Cross':
                            cardsa[q-1]=0
                        elif cardsa[q-1] == 'Square':
                            cardsa[q-1]=1
                        elif cardsa[q-1] == 'Triangle':
                            cardsa[q-1]=2
                        elif cardsa[q-1] == 'Circle':
                            cardsa[q-1]=3
                        elif cardsa[q-1] == 'Star':
                            cardsa[q-1]=4
                        elif cardsa[q-1] == 'Whot':
                            cardsa[q-1]=5
                        print int(cardsa[q-2])
                        print int(cardsa[q-1])
                        print "ok2 end"
                        self.hand2.remove_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        self.comp.add_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        del self.comp.cards[0]
                        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                        anim += Animation(opacity=1,  duration=0.8)
                        anim.start(self.btn1)
                        #time.sleep(1)
                        for a in self.comp.cards:
                        
                            self.btn1.background_normal='whot/'+str(a)+'.png'
                    #anim.cancel(self.btn1)
                        for child in self.handgrid1.children[:]:
                            self.handgrid1.remove_widget(child)
                        print len(self.hand2.cards)
                        for a in self.hand2.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                            self.handgrid1.add_widget(self.btu)
                            #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print len(self.hand2.cards)
                        self.turn = 2
                        time.sleep(1)
                        break
                                                
                        
                                
                                
                    elif int(cardsa[q-2])==1:
                        self.turn=2
                        print int(cardsa[q-2])
                        print 'that is y 1'
                        
                        print "ok2"
                        print q
                        print w
                        if cardsa[q-1] == 'Cross':
                            cardsa[q-1]=0
                        elif cardsa[q-1] == 'Square':
                            cardsa[q-1]=1
                        elif cardsa[q-1] == 'Triangle':
                            cardsa[q-1]=2
                        elif cardsa[q-1] == 'Circle':
                            cardsa[q-1]=3
                        elif cardsa[q-1] == 'Star':
                            cardsa[q-1]=4
                        elif cardsa[q-1] == 'Whot':
                            cardsa[q-1]=5
                        print int(cardsa[q-2])
                        print int(cardsa[q-1])
                        print "ok2 end"
                        self.hand2.remove_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        self.comp.add_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        del self.comp.cards[0]
                        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                        anim += Animation(opacity=1,  duration=0.8)
                        anim.start(self.btn1)
                        #time.sleep(1)
                        for a in self.comp.cards:
                        
                            self.btn1.background_normal='whot/'+str(a)+'.png'
                    #anim.cancel(self.btn1)
                        for child in self.handgrid1.children[:]:
                            self.handgrid1.remove_widget(child)
                        print len(self.hand2.cards)
                        for a in self.hand2.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                            self.handgrid1.add_widget(self.btu)
                            #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print len(self.hand2.cards)
                        self.turn = 2
                        time.sleep(1)
                        break
                    # waith
                    
                   
                    else :
                   
                        print "ok2"
                        print q
                        print w
                        if cardsa[q-1] == 'Cross':
                            cardsa[q-1]=0
                        elif cardsa[q-1] == 'Square':
                            cardsa[q-1]=1
                        elif cardsa[q-1] == 'Triangle':
                            cardsa[q-1]=2
                        elif cardsa[q-1] == 'Circle':
                            cardsa[q-1]=3
                        elif cardsa[q-1] == 'Star':
                            cardsa[q-1]=4
                        elif cardsa[q-1] == 'Whot':
                            cardsa[q-1]=5
                        print int(cardsa[q-2])
                        print int(cardsa[q-1])
                        print "ok2 end"
                        self.hand2.remove_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        self.comp.add_card(Card(int(cardsa[q-1]),int(cardsa[q-2])))
                        del self.comp.cards[0]
                        anim = Animation(opacity=0.3,  t='out_bounce',duration=2.)
                        anim += Animation(opacity=1,  duration=0.8)
                        anim.start(self.btn1)
                        #time.sleep(1)
                        for a in self.comp.cards:
                        
                            self.btn1.background_normal='whot/'+str(a)+'.png'
                    #anim.cancel(self.btn1)
                        for child in self.handgrid1.children[:]:
                            self.handgrid1.remove_widget(child)
                        print len(self.hand2.cards)
                        for a in self.hand2.cards:
                            self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                            self.handgrid1.add_widget(self.btu)
                           # self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                        print len(self.hand2.cards)
                        self.turn = 1
                        time.sleep(1)
                        break
            else : # change the turn
               
                
                self.hand2.add_card(self.deck.pop_card())
                for child in self.handgrid1.children[:]:
                    self.handgrid1.remove_widget(child)
                print len(self.hand2.cards)
                for a in self.hand2.cards:
                    self.btu=Button(markup=True,text='[color=666666][size=0]'+'!'+str(a)+'.png'+'![/size][/color]',background_normal='whot/'+'back.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )        
                    self.handgrid1.add_widget(self.btu)
                    #self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                print len(self.hand2.cards)
                print len(self.deck.cards)
                print len(self.hand2.cards)
                print q
                self.turn = 1
                time.sleep(1)
                    
            #abc.append(self.hand2.pop_card())
    def takefromhand(self):
        if len(self.deck.cards) <= 0:
            print 'end game'
        else:
            self.hand1.add_card(self.deck.pop_card())
            self.hand1.cards.reverse()
            for child in self.handgrid2.children[:]:
                self.handgrid2.remove_widget(child)
            print len(self.hand1.cards)
            for a in range(len(self.hand1.cards)):
                if a>=4:
                    break
                else:
                    abc=self.hand1.cards[a]
                    self.btu=Button(markup=True,text='[color=666666][size=10]'+'!'+str(abc)+'.png'+'![/size][/color]',background_normal='whot/'+str(abc)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )       
                    self.handgrid2.add_widget(self.btu)
                    self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
                    #time.sleep(1)
            print len(self.hand1.cards)
            print len(self.deck.cards)
            print len(self.hand1.cards)
            self.deal2.text='[color=333333][size=30]'+str(len(self.deck.cards))+'[/size][/color]'
        
        self.turn = 2
        
        
    def shufinhand(self):
        if len(self.hand1.cards) <= 4:
            pass
        else:
            self.hand1.cards.insert(0,self.hand1.pop_card())
            #self.hand1.cards.reverse()
            for child in self.handgrid2.children[:]:
                self.handgrid2.remove_widget(child)
            
            for a in range(len(self.hand1.cards)):
                if a>=4:
                    break
                else:
                    abc=self.hand1.cards[a]
                    self.btu=Button(markup=True,text='[color=666666][size=10]'+'!'+str(abc)+'.png'+'![/size][/color]',background_normal='whot/'+str(abc)+'.png',size_hint=(None,None),allow_stretch=True,size_hint_y=None, )       
                    self.handgrid2.add_widget(self.btu)
                    self.btu.bind(on_press= lambda a,b=self.btu.text: self.callback(b))
    def market(self, dt):
        self.deal2.text='[color=333333][size=30]'+str(len(self.deck.cards))+'[/size][/color]'
        self.bbtt.text='[color=333333][size=30]'+str(len(self.hand1.cards))+'[/size][/color]'
    def update(self,dt):
        if  len(self.deck.cards) <= 0 or len(self.hand1.cards) <= 0 or  len(self.hand2.cards)<=0:
            if len(self.hand1.cards) == 0:
            #self.remove_widget(self.decklayout)
                self.gridmain.remove_widget(self.handgrid1)
                
                self.gridmain.remove_widget(self.handgrid2)
                
                self.label.text='you win'
            elif len(self.hand2.cards) == 0:
            #self.remove_widget(self.decklayout)
                self.gridmain.remove_widget(self.handgrid1)
                
                self.gridmain.remove_widget(self.handgrid2)
                
                self.gridmain.label.text='computer win'
            
            
            else:
                self.gridmain.remove_widget(self.handgrid1)
                self.gridmain.remove_widget(self.handgrid2)
                cctv=[]
                cctv1=[]
                for w in self.hand1.cards:
                    wel=string.split(str(w), 'of')
                    cctv.append(int(wel[0]))
                p=sum(cctv)
                for wa in self.hand2.cards:
                    wel2=string.split(str(wa), 'of')
                    cctv1.append(int(wel2[0]))
                o=sum(cctv1)
                
                if p < o:
                    self.label.text='player 1 win' + str(p)
                else:
                    self.label.text='computer  win' + str(o)
                
            
            
            
        else:
            if self.turn == 1:
                self.label.text='player 1 turn'
                
            elif self.turn == 2:
                self.label.text='computers turn'
                
            self.trythis(dt)
            self.market(dt)
            
class main123(Screen):
    def __init__(self,**kwargs):
        super(main123, self).__init__(**kwargs)    
        self.gmain=GridLayout(rows=1)
        self.labs=Label(text='pls press down')
        self.gmain.add_widget(self.labs)
        self.video = Label(text='pls press down')
        self.add_widget(self.video)
        #self.video.play=True
        self.add_widget(self.gmain)
        
    def on_touch_down(self, touch):
        sm.current ='game'
        self.video.play=False
    

sm = ScreenManager(transition=FallOutTransition(),duration=.4) 
sm.add_widget(main123(name='menu')) 
sm.add_widget(game(name='game'))



         
class MyApp(App):
    icon='images/icon.png'
    title='MOBILE LEARN'
    def build(self):
        #games=game(name='game')
        #Window.fullscreen = True
        
        
        
        
        
        return sm
        
            



if __name__ == '__main__':
     MyApp().run()


